<?php

class Viabill_Quickpay_Helper_Order extends Mage_Core_Helper_Abstract {
    public function isQuickpayOrder($order) {
        return $order->getPayment()->getMethod() == 'viabillquickpay_payment';
    }
}
