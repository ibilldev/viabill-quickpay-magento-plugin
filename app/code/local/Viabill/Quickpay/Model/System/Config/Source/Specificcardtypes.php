<?php

class Viabill_Quickpay_Model_System_Config_Source_Specificcardtypes
{

  public function toOptionArray()
  {
    return array(
      array('value' => 'american-express', 'label' => Mage::helper('viabillquickpay')->__('American Express')),
      array('value' => 'american-express-dk', 'label' => Mage::helper('viabillquickpay')->__('American Express (udstedt i Danmark)')),
      array('value' => 'dankort', 'label' => Mage::helper('viabillquickpay')->__('Dankort')),
      array('value' => 'danske-dk', 'label' => Mage::helper('viabillquickpay')->__('Danske Net Bank')),
      array('value' => 'diners', 'label' => Mage::helper('viabillquickpay')->__('Diners Club')),
      array('value' => 'diners-dk', 'label' => Mage::helper('viabillquickpay')->__('Diners Club (udstedt i Danmark)')),
      array('value' => 'edankort', 'label' => Mage::helper('viabillquickpay')->__('eDankort')),
      array('value' => 'fbg1886', 'label' => Mage::helper('viabillquickpay')->__('Forbrugsforeningen af 1886')),
      array('value' => 'jcb', 'label' => Mage::helper('viabillquickpay')->__('JCB')),
      array('value' => 'mastercard', 'label' => Mage::helper('viabillquickpay')->__('Mastercard')),
      array('value' => 'mastercard-dk', 'label' => Mage::helper('viabillquickpay')->__('Mastercard (udstedt i Danmark)')),
      array('value' => 'mastercard-debet-dk', 'label' => Mage::helper('viabillquickpay')->__('Mastercard Debet (udstedt i Danmark)')),
      array('value' => 'mobilepay', 'label' => Mage::helper('viabillquickpay')->__('Mobilepay')),
      array('value' => 'nordea-dk', 'label' => Mage::helper('viabillquickpay')->__('Nordea Net Bank')),
      array('value' => 'visa', 'label' => Mage::helper('viabillquickpay')->__('Visa')),
      array('value' => 'visa-dk', 'label' => Mage::helper('viabillquickpay')->__('Visa (udstedt i Danmark)')),
      array('value' => 'visa-electron', 'label' => Mage::helper('viabillquickpay')->__('Visa Electron')),
      array('value' => 'visa-electron-dk', 'label' => Mage::helper('viabillquickpay')->__('Visa Electron (udstedt i Danmark)')),
      array('value' => 'paypal', 'label' => Mage::helper('viabillquickpay')->__('PayPal')),
      array('value' => 'sofort', 'label' => Mage::helper('viabillquickpay')->__('Sofort')),
      array('value' => 'viabill', 'label' => Mage::helper('viabillquickpay')->__('Viabill'))
    );
  }

}
