<?php
class Viabill_Quickpay_Model_System_Config_Source_Cardlogos
{

  public function toOptionArray()
  {
    return array(
      array('value' => 'dankort', 'label' => Mage::helper('viabillquickpay')->__('Dankort')),
      array('value' => 'edankort', 'label' => Mage::helper('viabillquickpay')->__('eDankort')),
      array('value' => 'danskenetbetaling', 'label' => Mage::helper('viabillquickpay')->__('Danske Netbetaling')),
      array('value' => 'nordea', 'label' => Mage::helper('viabillquickpay')->__('Nordea e-betaling')),
      array('value' => 'ewire', 'label' => Mage::helper('viabillquickpay')->__('EWIRE')),
      array('value' => 'forbrugsforeningen', 'label' => Mage::helper('viabillquickpay')->__('Forbrugsforeningen')),
      array('value' => 'visa', 'label' => Mage::helper('viabillquickpay')->__('VISA')),
      array('value' => 'visaelectron', 'label' => Mage::helper('viabillquickpay')->__('VISA Electron')),
      array('value' => 'mastercard', 'label' => Mage::helper('viabillquickpay')->__('MasterCard')),
      array('value' => 'maestro', 'label' => Mage::helper('viabillquickpay')->__('Maestro')),
      array('value' => 'jcb', 'label' => Mage::helper('viabillquickpay')->__('JCB')),
      array('value' => 'diners', 'label' => Mage::helper('viabillquickpay')->__('Diners Club')),
      array('value' => 'amex', 'label' => Mage::helper('viabillquickpay')->__('AMEX')),
      array('value' => 'sofort', 'label' => Mage::helper('viabillquickpay')->__('Sofort')),
      array('value' => 'viabill', 'label' => Mage::helper('viabillquickpay')->__('ViaBill')),
      array('value' => 'mobilepay', 'label' => Mage::helper('viabillquickpay')->__('Mobilepay')),
    );
  }

}
