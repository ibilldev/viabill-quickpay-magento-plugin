<?php
class Viabill_Quickpay_Model_System_Config_Source_Trustedlogos
{

  public function toOptionArray()
  {
    return array(
      array('value' => 'verisign_secure', 'label' => Mage::helper('viabillquickpay')->__('Verified by VISA')),
      array('value' => 'mastercard_securecode', 'label' => Mage::helper('viabillquickpay')->__('MasterCard Secure Code')),
      array('value' => 'pci', 'label' => Mage::helper('viabillquickpay')->__('PCI')),
      array('value' => 'nets', 'label' => Mage::helper('viabillquickpay')->__('Nets')),
      array('value' => 'euroline', 'label' => Mage::helper('viabillquickpay')->__('Euroline')),
    );
  }

}
