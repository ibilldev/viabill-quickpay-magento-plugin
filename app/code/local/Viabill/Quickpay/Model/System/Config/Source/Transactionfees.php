<?php

class Viabill_Quickpay_Model_System_Config_Source_Transactionfees
{

  public function toOptionArray()
  {
    return array(
      // DANSKE
      array('value' => 'american-express-dk', 'label' => Mage::helper('viabillquickpay')->__('American Express (Dansk)')),
      array('value' => 'dankort', 'label' => Mage::helper('viabillquickpay')->__('Dankort')),
      array('value' => 'diners-dk', 'label' => Mage::helper('viabillquickpay')->__('Diners (Dansk)')),
      array('value' => 'edankort', 'label' => Mage::helper('viabillquickpay')->__('edankort')),
      array('value' => 'maestro-dk', 'label' => Mage::helper('viabillquickpay')->__('Maestro (Dansk)')),
      array('value' => 'mastercard-dk', 'label' => Mage::helper('viabillquickpay')->__('Mastercard (Dansk)')),
      array('value' => 'mastercard-debet-dk', 'label' => Mage::helper('viabillquickpay')->__('Mastercard debit (Dansk)')),
      array('value' => 'mobilepay', 'label' => Mage::helper('viabillquickpay')->__('Mobilepay')),
      array('value' => 'visa-dk', 'label' => Mage::helper('viabillquickpay')->__('Visa (Dansk)')),
      array('value' => 'visa-electron-dk', 'label' => Mage::helper('viabillquickpay')->__('Visa Electron (Dansk)')),
      array('value' => 'fbg1886', 'label' => Mage::helper('viabillquickpay')->__('Forbrugsforeningen')),

      // UDENLANSKE
      array('value' => 'american-express', 'label' => Mage::helper('viabillquickpay')->__('American Express')),
      array('value' => 'diners', 'label' => Mage::helper('viabillquickpay')->__('Diners')),
      array('value' => 'jcb', 'label' => Mage::helper('viabillquickpay')->__('JCB')),
      array('value' => 'maestro', 'label' => Mage::helper('viabillquickpay')->__('Maestro')),
      array('value' => 'mastercard', 'label' => Mage::helper('viabillquickpay')->__('Mastercard')),
      array('value' => 'visa', 'label' => Mage::helper('viabillquickpay')->__('Visa')),
      array('value' => 'visa-electron', 'label' => Mage::helper('viabillquickpay')->__('Visa Electron'))


    );
  }

}
