<?php
class Viabill_Quickpay_Block_Payment_Form extends Mage_Payment_Block_Form
{
  protected function _construct()
  {
    $this->setTemplate('viabillquickpay/payment/form.phtml');
    parent::_construct();
  }

   public function isShowIcon(){
        return Mage::getStoreConfig('payment/viabillquickpay_payment/show_icon');
    }
  
}
