# ViaBill QuickPay Module  #
----------------------------
A module to be integrated in Magento WebShop for providing Payment Option and not a Payment Gateway.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
-----
- version: 1.0.1
- Module on BitBucket(https://bitbucket.org/ibilldev/viabill-quickpay-magento-plugin/)


###Description###
-----------
Pay using ViaBill. 
Install this Module in to your Magento Web Shop to provide an option to pay using ViaBill through QuickPay Payment Gateway.

###Requirements###
------------
* PHP >= 5.2.0

###Compatibility###
-------------
* Magento = 1.4, 1.5, 1.6, 1.7, 1.8, 1.9 

###Integration Instructions :  Using Magento Connect ### 
-------------------------
1. Download the Module from the bitbucket. 
2. Extract the module and look for a zip file viz. Viabill_Quickpay-1.0.1
3. Login to the Magento Admin Panel. Navigate to System->Magento Connect->Magento Connect Manager
4. Upload Package File : Browse and select the zip file to upload. 
   Please check the image below for reference :

![quickpay-magento-browse.PNG](https://bitbucket.org/repo/M4zeod/images/2954257407-quickpay-magento-browse.PNG)

![quickpay-magento-refresh.PNG](https://bitbucket.org/repo/M4zeod/images/3202351478-quickpay-magento-refresh.PNG)

5. Once upload is successful, navigate to System->Configuration->Payment Methods
6. Check the installed module and configure.
7. Done


###Integration Instructions :  Using FTP  ### 
----------------------------------------------

1. Download the module from bitbucket.

2. Extract the folder and unzip the folder named : Viabill_Quickpay-1.0.1

3. Within the 'app' folder, follow the folder structure and place the folder named 'Viabill' inside your app/code/local.

4. Extract app/design/adminhtml/default/default/layout/Viabill_Quickpay.xml  and place it inside your app/design/adminhtml/default/default/layout/

5. Extract app/design/adminhtml/default/default/template/viabillquickpay and place it inside your app/design/adminhtml/default/default/template/

6. Extract app/design/frontend/base/default/layout/Viabill_Quickpay.xml and place it inside your app/design/frontend/base/default/layout/

7. Extract app/design/frontend/base/default/template/viabillquickpay and place it inside your app/design/frontend/base/default/template/

8. Extract app/etc/modules/Viabill_Quickpay.xml and place it in your app/etc/modules/

9. Copy app/locale folder into your app/ folder

10. Copy js/ folder inside your project's js folder 

11. Copy contents of skin/ folder inside your project's skin/ folder

12. Clear the cache from Admin Panel: System->Configuration->Cache Management->Select All - Actions:Refresh - Submit.

13. Logout from the admin panel and then login again.

14. Configure and activate the module under System->Configuration->Advanced. Check in Disable Modules Output -> Viabill_Quickpay -> Enable.

15. Go to Admin->System->Configuration->Payment Methods. Click On ViaBill betaling | QuickPay.

16. Done.



#Uninstallation - Automatic 
--------------
1. Admin->System->Magento Connect->Magento Connect Manager
2. Look for Viabill_Quickpay in installed modules section.
3. Select Uninstall -- Commit Changes


#Uninstallation - Manual 
--------------
1. Remove Folder "Viabill" from app/code/local
2. Remove viabillquickpay folder from app/design/adminhtml/default/default/template/
3. Remove the Viabill_Quickpay.xml file from app/etc/modules
4. Remove viabillquickpay folder from app/design/frontend/base/default/template/
5. Remove Viabill_Quickpay.xml from your app/design/adminhtml/default/default/layout/
6. Remove Viabill_Quickpay.xml from your app/design/frontend/base/default/layout/



#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)